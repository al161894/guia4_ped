﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA4_PED_FORM
{
    public partial class Form2 : Form
    {
        Queue<Empleados> Trabajadores = new Queue<Empleados>();
        //creamos objeto de la clase cola, del tipo de la clase empleado (lo que almacena son objetos)

        public Form2()
        {
            InitializeComponent();
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            Empleados empleado = new Empleados();//creamos instancia de la clase empleado
            //capturamos los datos del empleado  
            if(txtcarnet.Text == null)
            {
                errorProvider1.SetError(txtcarnet, "Campo requerido");
            }
            if (txtnombre.Text == null)
            {
                errorProvider1.SetError(txtnombre, "Campo requerido");
            }
            if (txtsalario.Text == null)
            {
                errorProvider1.SetError(txtcarnet, "Campo requerido");
            }else if(int.Parse(txtsalario.Text) < 0)
            {
                errorProvider1.SetError(txtcarnet, "Solo números positivos");
            }
            empleado.Carnet = txtcarnet.Text;
            empleado.Nombre = txtnombre.Text;
            empleado.Salario = Decimal.Parse(txtsalario.Text);
            empleado.Fecha = dtpfecha.Value;
            Trabajadores.Enqueue(empleado);//llamamos al metodo encolar para meter a la estructura
            dgvCola.DataSource = null;//iniciamos el dgv con null
            dgvCola.DataSource = Trabajadores.ToArray();//para pasarlo al dgv covertimos la cola en array
            Limpiar();//se limpian los txt's
            txtcarnet.Focus();//se coloca el cursor sobre el primer textbox

        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            if(Trabajadores.Count != 0)//mientras haya trabajadores en la cola
            {
                Empleados empleado = new Empleados();//instancia de la clase empleado
                                                     /*Este objeto se usa para poder recuperar los datos y mostrarlos en los txt's
                                                      * al momento de ser eliminados de la cola*/
                empleado = Trabajadores.Dequeue();//llamamos al metodo desencolar
                //colocamos los datos en sus textboxs
                txtcarnet.Text = empleado.Carnet;
                txtnombre.Text = empleado.Nombre;
                txtsalario.Text = empleado.Salario.ToString();
                dtpfecha.Value = empleado.Fecha;
                //la estructura convertida en lista se le pasa al dgv (ahora tiene un empleado menos)
                dgvCola.DataSource = Trabajadores.ToList();
                MessageBox.Show("Se eliminó el registro en cola", "AVISO");
                Limpiar();
            }
            else
            {
                MessageBox.Show("No hay empleados en la cola", "AVISO");
                Limpiar();
            }
            txtcarnet.Focus();
        }

        public void Limpiar()//limpia los txt
        {
            txtcarnet.Clear();
            txtnombre.Clear();
            txtsalario.Clear();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Application.Exit();//salir de la aplicacion
        }

        private void txtcarnet_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                errorProvider1.SetError(txtcarnet, "Digite solo números o letras");
            }            
        }

        private void txtsalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                errorProvider1.SetError(txtsalario, "Digite solo números");
            }
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                errorProvider1.SetError(txtsalario, "Digite solo letras");
            }
        }
    }
}
