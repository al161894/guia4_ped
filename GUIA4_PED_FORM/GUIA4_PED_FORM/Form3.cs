﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA4_PED_FORM
{
    public partial class Form3 : Form
    {
        //instancia de las colas
        Queue<int> fila1 = new Queue<int>();
        Queue<int> fila2 = new Queue<int>();
        Queue<int> fila3 = new Queue<int>();
        Queue<int> fila4 = new Queue<int>();
        
        int[,] filasCant = new int[4,1];
        //declarando variable        

        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {                       

            if(fila1.Count == 0 && fila2.Count == 0 && fila3.Count == 0 && fila4.Count == 0)
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila1.Enqueue(p.persona);

                listBox1.DataSource = fila1.ToArray();                
            }
            else if(fila1.Count < fila2.Count  || fila1.Count < fila3.Count || fila1.Count < fila4.Count)
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila1.Enqueue(p.persona);

                listBox1.DataSource = fila1.ToArray();
            }
            else if (fila2.Count < fila1.Count || fila2.Count < fila3.Count || fila2.Count < fila4.Count)
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila2.Enqueue(p.persona);

                listBox2.DataSource = fila2.ToArray();
            }
            else if (fila3.Count < fila1.Count || fila3.Count < fila2.Count || fila3.Count < fila4.Count)
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila3.Enqueue(p.persona);

                listBox3.DataSource = fila3.ToArray();
            }
            else if (fila4.Count < fila1.Count || fila4.Count < fila2.Count || fila4.Count < fila3.Count)
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila4.Enqueue(p.persona);

                listBox4.DataSource = fila4.ToArray();
            }
            else
            {
                Cola1 p = new Cola1();
                p.persona = 1;
                fila1.Enqueue(p.persona);

                listBox1.DataSource = fila1.ToArray();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fila1.Dequeue();
            listBox1.DataSource = fila1.ToArray();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            fila2.Dequeue();
            listBox1.DataSource = fila2.ToArray();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            fila3.Dequeue();
            listBox1.DataSource = fila3.ToArray();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            fila4.Dequeue();
            listBox1.DataSource = fila4.ToArray();
        }
    }
}
